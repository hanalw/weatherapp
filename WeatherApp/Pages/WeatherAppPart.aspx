﻿<%@ Page language="C#" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<WebPartPages:AllowFraming ID="AllowFraming" runat="server" />
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link href="../Content/App.css" rel="stylesheet" />
     <link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet">
    <script type="text/javascript" src="../Scripts/jquery-3.4.1.min.js"></script>
    <title></title>
    <script src="../Scripts/moment.min.js"></script>
    <script src="../Scripts/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/sv.js"></script>
    <script src="../Scripts/App.js"></script>
</head>
<body>

    <div class="container">


        <div class="topPart">
            <div>
            <img class="backgroundPicture" src="../Images/bg.png" alt="bg"/>
            <p class="location">London, England</p>
            <div class="degrees">
                <p id="degre">28°C</p> 
                
                <div class="highLow">
                    <div>
                        <span class="highLow-label">L</span>
                        <span id="highlo">8°</span>
                    </div>
                    <div>
                        <span class="highLow-label">H</span>
                        <span id="highhigh">18°</span>
                    </div>
                   </div>
                <div class="middlePart">
                    <div class="weatherPrognos">Soligt</div>
                    <span><--(<span class="windInfo"></span>)m/s</span>
                <div class="middleRow">
                    <span class="dateDay">Måndag</span>|
                    <span class="dateInfo">2 Mars</span>|
                    <span class="timeNow">11:01</span>
                </div>   
                </div>
            </div> 
                </div>
        </div>

        <div class="bottomPart">
            <div class="row-part">
            <div class="row-item">
                <div class="row-item-one">Sol</div>
                <div class="columnOne">
                    <span id="dagett">Imorgon</span>
                    <span></span>
                    <p></p>
                </div>
                <div>
                    <div class="columnTwo">
                        <span>L</span>
                        <span id="columnTwoLL">10</span>
                    </div>
                    <div class="columnThree">
                        <span>H</span>
                        <span id="columnTwoHH">15</span>
                    </div>
                </div>
            </div>
            </div>

            <div class="row-part">
            <div class="row-item">
                <div class="row-item-two">Sol</div>
                <div class="columnOne">
                    <span id="dagtva">Imorgon</span>
                    <span></span>
                    <p></p>
                </div>
                <div>
                    <div class="columnTwo">
                        <span>L</span>
                        <span id="columnTwoL">10</span>
                    </div>
                    <div class="columnThree">
                        <span>H</span>
                        <span id="columnTwoH">15</span>
                    </div>
                </div>
            </div>
             </div>

            <div class="row-part">
            <div class="row-item">
                <div class="row-item-three">Sol</div>
                <div class="columnOne">
                    <span id="dagtre">Imorgon</span>
                    <span></span>
                    <p></p>
                </div>
                <div>
                    <div class="columnTwo">
                        <span >L</span>
                        <span id="dagtreL">10</span>
                    </div>
                    <div class="columnThree">
                        <span>H</span>
                        <span id="dagtreH">15</span>
                    </div>
                </div>
            </div> 
            </div>

            <div class="row-part">
            <div class="row-item">
                <div class="row-item-four">Sol</div>
                <div class="columnOne">
                    <span id="dagfyra">Imorgon</span>
                    <span></span>
                    <p></p>
                </div>
                <div>
                    <div class="columnTwo">
                        <span id="dagfyraL">L</span>
                        <span>10</span>
                    </div>
                    <div class="columnThree">
                        <span>H</span>
                        <span id="dagfyraH">15</span>
                    </div>
                </div>
            </div> 
            </div>

            <div class="row-part">
            <div class="row-item">
                <div class="row-item-five">Sol</div>
                <div class="columnOne">
                    <span id="dagFem">Imorgon</span>
                    <span></span>
                    <p></p>
                </div>
                <div>
                    <div class="columnTwo">
                        <span>L</span>
                        <span id="dagFemL">10</span>
                    </div>
                    <div class="columnThree">
                        <span>H</span>
                        <span id="dagFemH">15</span>
                    </div>
                </div>
                </div>

                   <p id="button-link"><a href="https://ecusolna-503e5d9fd8bc10.sharepoint.com/sites/spo19-dev3/WeatherApp/" target="_blank">GÅ TILL FULLPAGE</a></p>

            </div> 
        
    </div>
        </div>
</body>
</html>