﻿<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="text/javascript" src="../Scripts/Helper.js"></script>
    <script type="text/javascript" src="../Scripts/chartVariables.js"></script>
    <script type="text/javascript" src="../Scripts/FullpageScript.js"></script>
    <SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <meta name="WebPartPageExpansion" content="full" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet"/>
    <link rel="Stylesheet" type="text/css" href="../Content/Style.css" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

   <div class="container">
	<section class="top-part" style="background-image: url('../Images/Fullpage/sun.png');">
      <div class="top-part-content">
        <p class="top-part-text"><span>Idag</span><span>feb 25</span><span>13:24</span></p>
        <div class="weather-info">
          <div class="weahter-info-header">
            <p>Lidingö, Stockholm</p>
            <div class="weather-info-weather">
              <img class="svg" src="../Icons/sun.svg" alt="icon"/>
              <span>15°C</span>
              <div class="lowHigh">
                <div>
                <span>L</span>
                <span>10°</span>
              </div>
              <div>
                <span>H</span>
                <span>15°</span>
              </div>
              </div>
            </div>
            <span class="clearfix"></span>
          </div>
          <div class="weather-info-body">
            <div class="weather-info-details">
              <div>
                <img class="svg" src="../Icons/arrow.svg" alt="icon" />
                <span class="yellow-text">5(7)m/s</span>
              </div>
              <span>Vind(byvind)</span>
            </div>
            <div class="weather-info-details">
              <span class="yellow-text">78%</span>
              <span>Luftfuktighet</span>
            </div>
            <div class="weather-info-details">
              <span class="yellow-text">998hPa</span>
              <span>Lufttryck</span>
            </div>
            <div class="weather-info-details">
              <span class="yellow-text">20g/cm³</span>
              <span>Ozonvärde</span>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="graph-section">
      <div class="tabs">
        <p class="tab active" data-tab="perHour">Timprognos</p>
        <p class="tab" data-tab="8-forecast">8-dygnsprognos</p>
      </div>
      <div class="graph-content active" id="perHour">
        <h2 class="section-heading">Temperatur per timme</h2>
        <div class="chart-container">
          <canvas class="chart" id="chartPerHour" width="1360" height="415">
            Chart kan inte visas
          </canvas>
          <div id="hourby-legend"></div>
        </div>
      </div>
      <div class="graph-content" id="8-forecast">
        <h2 class="section-heading">8-dygnsprognos</h2>
          <div class="chart-container">
            <canvas class="chart" id="chartPerDay" width="1360" height="415">
            Chart kan inte visas
          </canvas>
        </div>
      </div>
    </section>
	</div>

</asp:Content>