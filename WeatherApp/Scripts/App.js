﻿$(document).ready(function () {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api.opencagedata.com/geocode/v1/json?q=" + locationen + "&key=9e4c34260190445cb8b74233f1ab5fec&language=sv&pretty=1&no_annotations=1",
        "method": "GET",
        "dataType": 'json',
        success: function (data) {
            var latitude = data.results[0].bounds.northeast.lat;
            var longitude = data.results[0].bounds.northeast.lng;
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/27bc208c09d5f6391f9806bc117b728f/" + latitude + "," + longitude + "/?exclude=hourly,alerts&" + celciusOrFarhenheit + "&" + language + "",
                "method": "GET",
                "dataType": 'json',
                success: function (data) {
                    var currentTime = data.currently.time;
                    var currentSummary = data.currently.summary;
                    var ikon = data.currently.icon;
                    var precipintensitet = data.currently.precipIntensity;
                    var precipProbability = data.currently.precipProbability;
                    var precipType = data.currently.precipType;
                    var precipAccumulation = data.currently.precipAccumulation;
                    var temperature = data.currently.temperature;
                    var apparentTemperature = data.currently.temperature;
                    var dewPoint = data.currently.dewPoint;
                    var humidity = data.currently.humidity;
                    var pressure = data.currently.pressure;
                    var windSpeed = data.currently.windSpeed;
                    var windGust = data.currently.windGust;
                    var windBearing = data.currently.windBearing;
                    var cloudCover = data.currently.cloudCover;
                    var uvIndex = data.currently.uvIndex;
                    var visibility = data.currently.visibility;
                    var ozone = data.currently.ozone;

                    var veckoData = data.daily.data;
                    var dayOneIcon = veckoData[0].icon;
                    var dayTwoIcon = veckoData[1].icon;
                    var dayThreeIcon = veckoData[2].icon;
                    var dayFourIcon = veckoData[3].icon;
                    var dayFiveIcon = veckoData[4].icon;
                    var dayOne = veckoData[0].time;
                    var dayTwo = veckoData[1].time;
                    var dayThree = veckoData[2].time;
                    var dayFour = veckoData[3].time;
                    var dayFive = veckoData[4].time;
                    var highDayOne = veckoData[0].temperatureHigh;
                    var highDayTwo = veckoData[1].temperatureHigh;
                    var highDayThree = veckoData[2].temperatureHigh;
                    var highDayFour = veckoData[3].temperatureHigh;
                    var highDayFive = veckoData[4].temperatureHigh;
                    var lowDayOne = veckoData[0].temperatureLow;
                    var lowDayTwo = veckoData[1].temperatureLow;
                    var lowDayThree = veckoData[2].temperatureLow;
                    var lowDayFour = veckoData[3].temperatureLow;
                    var lowDayFive = veckoData[4].temperatureLow;
                    var dagOne = moment.unix(dayOne).format('dddd');
                    var dagett = moment.unix(dayOne).format('dddd Do YYYY');
                    var dagTwo = moment.unix(dayTwo).format('dddd Do YYYY');
                    var dagThree = moment.unix(dayThree).format('dddd Do YYYY');
                    var dagFour = moment.unix(dayFour).format('dddd Do YYYY');
                    var dagFive = moment.unix(dayFive).format('dddd Do YYYY');
                    var dagOneMonth = moment.unix(dayOne).format('Do MMMM');
                    var clock = moment().format('h:mm ');

                    $('.location').text(locationen);
                    $('#degre').text(temperature);
                    $('#highlo').text(apparentTemperature);
                    $('#highhigh').text(highDayOne);
                    $('.weatherPrognos').text(currentSummary);
                    $('.windInfo').text(windSpeed);
                    $('.dateDay').text(dagOne);
                    $('.dateInfo').text(dagOneMonth);
                    $('.timeNow').text(clock);
                    $('.row-item-one').text(dayOneIcon);
                    $('#dagett').text(dagett);
                    $('#columnTwoL').text(lowDayOne);
                    $('#columnTwoH').text(highDayOne);
                    $('.row-item-two').text(dayTwoIcon);
                    $('#dagtva').text(dagTwo);
                    $('#columnTwoLL').text(lowDayTwo);
                    $('#columnTwoHH').text(highDayTwo);
                    $('.row-item-three').text(dayThreeIcon);
                    $('#dagtre').text(dagThree);
                    $('#dagtreL').text(lowDayThree);
                    $('#dagtreH').text(highDayThree);
                    $('.row-item-four').text(dayFourIcon);
                    $('#dagfyra').text(dagFour);
                    $('#dagfyraL').text(lowDayFour);
                    $('#dagfyraH').text(highDayFour);
                    $('.row-item-five').text(dayFiveIcon);
                    $('#dagFem').text(dagFive);
                    $('#dagFemL').text(lowDayFive);
                    $('#dagFemH').text(highDayFive);
                }
            }
            )
        }
    }
    $.ajax(settings)
});
function getQueryStringParameter(urlParameterKey) {
    var params = document.URL.split('?')[1].split('&');
    var strParams = '';
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split('=');
        if (singleParam[0] == urlParameterKey)
            return decodeURIComponent(singleParam[1]);
    }
}
var longitude;
var latitude;
var settings;
var language;
var celciusOrFarhenheit;
var languangeVal;
var languangeVal = getQueryStringParameter('MyEnum');
if (languangeVal == 1) {
    var language = "&lang=sv"
}
else if (languangeVal == 2) {
    var language = "&lang=en"
}
else if (languangeVal == 3) {
    var language = "&lang=es"
}
var chooseDegrees = getQueryStringParameter('MyBool');
if (chooseDegrees === 'false') {
    var celciusOrFarhenheit = "&units=us";
}
else if (chooseDegrees === 'true') {
    var celciusOrFarhenheit = "&units=si"
}
var locationen = getQueryStringParameter('MyString');