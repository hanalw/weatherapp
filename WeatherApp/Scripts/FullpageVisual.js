$(document).ready(function() {
	$('.tab').on('click', function() {
	 const tabId = $(this).attr('data-tab') 

   $('.tab').removeClass('active')
   $('.graph-content').removeClass('active')

   $(this).addClass('active')
   $(`#${tabId}`).addClass('active')
	})
})

// graph
var hourChart = document.getElementById('chartPerHour').getContext("2d")

// gradients
var lowChart = hourChart.createLinearGradient(100, 100, 100, 500);
var highChart = hourChart.createLinearGradient(100, 100, 100, 500);

lowChart.addColorStop(0, "rgba(0, 193, 210, 0.8)");
lowChart.addColorStop(1, "rgba(0, 193, 210, 0)");
highChart.addColorStop(0, "rgba(253, 54, 54, 0.8)");
highChart.addColorStop(1, "rgba(253, 54, 54, 0)");

var myChart = new Chart(hourChart, {
  type: 'line',
  data: {
    labels: ["01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00", "24:00", "01:00"],
    datasets: [
      {
        label: "Lägst Temperatur",
        lineTension: 0.5,
        borderColor: "#00C1D2",
        pointBorderColor: "#00C1D2",
        pointBackgroundColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "#00C1D2",
        fill: true,
        backgroundColor: lowChart,
        data: [-5, 2, 4, 3, 5, 5, 3, 5, 4, 4, 5, 3, 4, -2, 4, 5, 3, 2, 1, 3, 4, 3, 2, 4, 3]
      },
      {
        label: "Högst Temperatur",
        lineTension: 0.5,
        borderColor: "#FF4040",
        pointBorderColor: "#FF4040",
        pointBackgroundColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "#FF4040",
        fill: true,
        backgroundColor: highChart,
        borderWidth: 2,
        data: [5, 6, 8, 6, 9, 7, 5, 8, 6, 8, 7, 5, 7, 5, 8, 7, 5, 6, 4, 6, 9, 5, 7, 10, 7]
      }
    ]
  },
  options: {
    legend: {
      legend: false,
      legendCallback: function(chart) {
        var legend = [];
        var item = chart.data.datasets[0];
        for (var i=0; i < item.data.length; i++) {
          legend.push('<span class="chart-legend" style="background-color:' + item.backgroundColor[i] +'"></span>');
          legend.push('<span class="chart-legend-label-text">' + item.data[i] + ' person - '+chart.data.labels[i]+' times</span>');
        }
        return legend.join("");
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: "#9A9A9A",
          padding: 10,
          suggestedMax: 40,
          suggestedMin: -10,
          stepSize: 10,
          callback: function(value, index, values) {
            return `${value}°C`;
          }
        },
        gridLines: {
          drawTicks: false,
          borderDash: [1, 6],
          color: "#9A9A9A",
          tickMarkLength: 10
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "#9A9A9A",
          padding: 10
        },
        gridLines: {
          drawTicks: false,
          display: false
        }
      }]
    }
  }
});

$('#legend').html(myChart.generateLegend());