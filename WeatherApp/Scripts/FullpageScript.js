﻿$(document).ready(function () {
  console.log($('#chartPerHour'))
  $('.tab').on('click', function () {
    const tabId = $(this).attr('data-tab')

    $('.tab').removeClass('active')
    $('.graph-content').removeClass('active')

    $(this).addClass('active')
    $(`#${tabId}`).addClass('active')
  })
})


window.onload = function () {

  // graphs
  var hourChart = document.getElementById('chartPerHour').getContext("2d")
  var dayChart = document.getElementById('chartPerDay').getContext("2d")

  // gradient
  const lowChartOne = hourChart.createLinearGradient(0, 0, 0, 450);
  const highChartOne = hourChart.createLinearGradient(0, 0, 0, 450);

  const lowChartTwo = hourChart.createLinearGradient(0, 0, 0, 450);
  const highChartTwo = hourChart.createLinearGradient(0, 0, 0, 450);

  lowChartOne.addColorStop(0, "rgba(0, 193, 210, 0.8)");
  lowChartOne.addColorStop(1, "rgba(0, 193, 210, 0)");
  highChartOne.addColorStop(0, "rgba(253, 54, 54, 0.8)");
  highChartOne.addColorStop(1, "rgba(253, 54, 54, 0)");

  lowChartTwo.addColorStop(0, "rgba(0, 193, 210, 0.8)");
  lowChartTwo.addColorStop(1, "rgba(0, 193, 210, 0)");
  highChartTwo.addColorStop(0, "rgba(253, 54, 54, 0.8)");
  highChartTwo.addColorStop(1, "rgba(253, 54, 54, 0)");

  var chartPerHour = new Chart(hourChart, {
    type: 'line',
    data: {
      labels: ["01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00", "24:00", "01:00"],
      datasets: [
        {
          label: 'Lägst Temperatur',
          borderColor: "#00C1D2",
          pointBorderColor: "#00C1D2",
          pointBackgroundColor: chartWhite,
          fill: true,
          backgroundColor: lowChartOne,
          data: [-5, 2, 4, 3, 5, 5, 3, 5, 4, 4, 5, 3, 4, -2, 4, 5, 3, 2, 1, 3, 4, 3, 2, 4, 3]
        },
        {
          label: 'Högst Temperatur',
          borderColor: "#FF4040",
          pointBorderColor: "#FF4040",
          pointBackgroundColor: chartWhite,
          fill: true,
          backgroundColor: highChartOne,
          data: [5, 6, 8, 6, 9, 7, 5, 8, 6, 20, 18, 15, 7, 5, 8, 7, 5, 6, 4, 6, 9, 5, 7, 10, 7]
        }
      ]
    },
    options: {
      legend: {
        /*display: true,
        legendCallback: function (chart) {
          let legend = [];
          const item = chart.data.datasets[0];
          for (var i = 0; i < item.data.length; i++) {
            legend.push('<div>')
            legend.push(`<span class="chart-legend" style="background-color:${item.data.datasets[i].borderColor}"></span>`);
            legend.push(`<span class="chart-legend-label-text">${chart.data.datasets[i].label}</span>`);
            legend.push('</div>')
          }
          return legend.join("");
        }*/
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontColor: chartGray,
            padding: chartPadding,
            suggestedMax: 40,
            suggestedMin: -10,
            stepSize: 10,
            callback: function (value) {
              return `${value}°C`;
            }
          },
          gridLines: {
            drawTicks: false,
            borderDash: [1, 6],
            color: chartGray,
            tickMarkLength: 10
          }
        }],
        xAxes: [{
          ticks: {
            fontColor: chartGray,
            padding: chartPadding
          },
          gridLines: {
            display: false
          }
        }]
      }
    }
  })

  // Chart per day
  var chartPerDay = new Chart(dayChart, {
    type: 'line',
    data: {
      labels: ['4 mars', '5 mars', '6 mars', '7 mars', '8 mars'],
      datasets: [
        {
          label: 'Lägst Temperatur',
          borderColor: "#00C1D2",
          pointBorderColor: "#00C1D2",
          pointBackgroundColor: chartWhite,
          fill: true,
          backgroundColor: lowChartTwo,
          data: [5, 6, 3, 5, 4]
        },
        {
          label: 'Högst Temperatur',
          borderColor: "#FF4040",
          pointBorderColor: "#FF4040",
          pointBackgroundColor: chartWhite,
          fill: true,
          backgroundColor: highChartTwo,
          data: [8, 10, 15, 8, 9]
        }
      ]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontColor: chartGray,
            padding: chartPadding,
            suggestedMax: 40,
            suggestedMin: -10,
            stepSize: 10,
            callback: function (value) {
              return `${value}°C`;
            }
          },
          gridLines: {
            drawTicks: false,
            borderDash: [1, 6],
            color: chartGray,
            tickMarkLength: 10
          }
        }],
        xAxes: [{
          ticks: {
            fontColor: chartGray,
            padding: chartPadding
          },
          gridLines: {
            display: false
          }
        }]
      }
    }
  })
}