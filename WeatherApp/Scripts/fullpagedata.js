﻿$(document).ready(function () {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://api.opencagedata.com/geocode/v1/json?q=" + locationen + "&key=9e4c34260190445cb8b74233f1ab5fec&language=sv&pretty=1&no_annotations=1",
        "method": "GET",
        "dataType": 'json',
        success: function (data) {
            var latitude = data.results[0].bounds.northeast.lat;
            var longitude = data.results[0].bounds.northeast.lng;
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/27bc208c09d5f6391f9806bc117b728f/" + latitude + "," + longitude + "/?exclude=alerts&ang=sv&units=si",
                "method": "GET",
                "dataType": 'json',
                success: function (data) {
                    var currentTime = data.currently.time;
                    var currentSummary = data.currently.summary;
                    var precipintensitet = data.currently.precipIntensity;
                    var precipProbability = data.currently.precipProbability;
                    var precipType = data.currently.precipType;
                    var precipAccumulation = data.currently.precipAccumulation;
                    var temperature = data.currently.temperature;
                    var apparentTemperature = data.currently.temperature;
                    var dewPoint = data.currently.dewPoint;
                    var humidity = data.currently.humidity;
                    var pressure = data.currently.pressure;
                    var windSpeed = data.currently.windSpeed;
                    var windGust = data.currently.windGust;
                    var windBearing = data.currently.windBearing;
                    var cloudCover = data.currently.cloudCover;
                    var uvIndex = data.currently.uvIndex;
                    var visibility = data.currently.visibility;
                    var ozone = data.currently.ozone;

                    var highPerTimme = data.hourly.data;
                    var lowPerTimme = data.hourly.data;

                    var veckoData = data.daily.data;
                    var dayOne = veckoData[0].time;
                    var dayTwo = veckoData[1].time;
                    var dayThree = veckoData[2].time;
                    var dayFour = veckoData[3].time;
                    var dayFive = veckoData[4].time;
                    var highDayOne = veckoData[0].temperatureHigh;
                    var highDayTwo = veckoData[1].temperatureHigh;
                    var highDayThree = veckoData[2].temperatureHigh;
                    var highDayFour = veckoData[3].temperatureHigh;
                    var highDayFive = veckoData[4].temperatureHigh;
                    var lowDayOne = veckoData[0].temperatureLow;
                    var lowDayTwo = veckoData[1].temperatureLow;
                    var lowDayThree = veckoData[2].temperatureLow;
                    var lowDayFour = veckoData[3].temperatureLow;
                    var lowDayFive = veckoData[4].temperatureLow;
                    var dagOne = moment.unix(dayOne).format('ddd');
                    var dagTwo = moment(dayTwo).format('ddd');
                    var dagThree = moment.unix(dayThree).format('ddd');
                    var dagFour = moment(dayFour).format('ddd');
                    var dagFive = moment(dayFive).format('ddd');
                    var highTemp = [];
                    var lowTemp = [];
                    $.each(highPerTimme, function () {
                        highTemp.push(this.temperature);
                    })
                    $.each(lowPerTimme, function () {
                        lowTemp.push(this.apparentTemperature);
                    })
                }
            }
            )
        }
    }
    $.ajax(settings)
});

var longitude;
var latitude;
var settings;
var locationen = "Stockholm"